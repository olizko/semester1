package ru.omsu.imit.course3.exercise1.threads.synchronize;

import ru.omsu.imit.course3.exercise1.threads.forlist.ListForThread;

public class ThreadAdd extends Thread {
    private ListForThread list;

    public ThreadAdd(ListForThread list) {
        this.list = list;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            synchronized (list) {
                list.listAdd(10000);
            }
        }
    }
}
