package ru.omsu.imit.course3.exercise1.threads.queuemultistage;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public class MultiExecutor extends Thread {
    private BlockingQueue<AbstractMultiStage> concurrentLinkedQueue;
    private CountDownLatch countDownLatch;

    public MultiExecutor(BlockingQueue<AbstractMultiStage> concurrentLinkedQueue, CountDownLatch countDownLatch) {
        this.concurrentLinkedQueue = concurrentLinkedQueue;
        this.countDownLatch = countDownLatch;
    }

    public void run() {
        while (true) {
            AbstractMultiStage task = concurrentLinkedQueue.poll();
            if (!task.isComplete()) {
                task.execution();
                concurrentLinkedQueue.add(task);
            }
            if (task.isTerminator()) {
                break;
            }
            countDownLatch.countDown();
        }
    }
}
