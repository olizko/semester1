package ru.omsu.imit.course3.exercise1.collections.institute;

import ru.omsu.imit.course3.exercise1.collections.institute.InstituteErrorCodes;
import ru.omsu.imit.course3.exercise1.collections.institute.InstituteException;

public class Institute {
    String InstituteName;
    String InstituteCity;

    public Institute(String InstituteName, String InstituteCity) throws InstituteException {
        this.InstituteName = InstituteName;
        this.InstituteCity = InstituteCity;
        checkInstisusteName(InstituteName);
        checkInstituteCity(InstituteCity);
    }

    public String getInstituteName() {
        return InstituteName;
    }

    public void setInstituteName(String instituteName) throws InstituteException {
        InstituteName = instituteName;
        checkInstisusteName(InstituteName);

    }

    public String getInstituteCity() {
        return InstituteCity;
    }

    public void setInstituteCity(String instituteCity) throws InstituteException {
        InstituteCity = instituteCity;
        checkInstituteCity(InstituteCity);
    }


    private static void checkInstisusteName(String InstituteName1) throws InstituteException {
        if (InstituteName1 == null || InstituteName1.length() == 0)
            throw new InstituteException(InstituteErrorCodes.WRONG_INSTITUTE_NAME, InstituteName1);
    }

    private static void checkInstituteCity(String InstituteCity1) throws InstituteException {
        if (InstituteCity1 == null || InstituteCity1.length() == 0)
            throw new InstituteException(InstituteErrorCodes.WRONG_INSTITUTE_CITY, InstituteCity1);
    }
}
