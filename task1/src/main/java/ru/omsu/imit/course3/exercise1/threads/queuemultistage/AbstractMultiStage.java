package ru.omsu.imit.course3.exercise1.threads.queuemultistage;

public abstract class AbstractMultiStage {
    abstract boolean isTerminator();
    abstract boolean isComplete();
    abstract void execution();
}
