package ru.omsu.imit.course3.exercise1.threads;

public class ThirdThread implements Runnable {
    private Thread thread;
    public ThirdThread() {
        thread = new Thread(this, "thread");
        System.out.println("Third thread: " + thread);
        thread.start();
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("Third Thread: " + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Third interrupted.");
        }
        System.out.println("Exit from third thread.");
    }
}
