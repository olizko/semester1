package ru.omsu.imit.course3.exercise1.threads.queuetask;

import java.util.concurrent.BlockingQueue;

public class Developer extends Thread {
    private BlockingQueue<AbstractTask> concurrentLinkedQueue;
    private int writer;

    public Developer(BlockingQueue<AbstractTask> concurrentLinkedQueue, int writer) {
        this.concurrentLinkedQueue = concurrentLinkedQueue;
        this.writer = writer;
    }

    public void run() {
        for (int i = 0; i < writer; i++) {
            concurrentLinkedQueue.add(new Task("i"));
        }
    }
}
