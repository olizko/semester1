package ru.omsu.imit.course3.exercise1.collections.group;

import ru.omsu.imit.course3.exercise1.firsttask.Trainee;

public class Group {
    String groupName;
    Trainee[] trainees;

    public Group(String groupName, Trainee[] trainees) throws GroupException {
        testGroupName(groupName);
        testTrainees(trainees);
        this.groupName = groupName;
        this.trainees = trainees;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) throws GroupException {
        this.groupName = groupName;
        testGroupName(groupName);
    }

    public Trainee[] getTrainees() {
        return trainees;
    }

    public void setTrainees(Trainee[] trainees) throws GroupException {
        this.trainees = trainees;
        testTrainees(trainees);
    }

    private static void testGroupName(String groupName1) throws GroupException {
        if (groupName1 == null || groupName1.length() == 0)
            throw new GroupException(GroupErrorCodes.WRONG_GROUP_NAME, groupName1);
    }

    private static void testTrainees(Trainee[] trainees1) throws GroupException {
        if (trainees1 == null || trainees1.length == 0)
            throw new GroupException(GroupErrorCodes.WRONG_TRAINEES, trainees1);
    }
}
