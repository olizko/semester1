package ru.omsu.imit.course3.exercise1.threads.forlist;

import java.util.List;
import java.util.Random;

public class ListForThread {
    private List<Integer> list;

    public ListForThread(List<Integer> list) {
        this.list = list;
    }

    public void listAdd(int n) {
        Random random = new Random(0);
        int randomIndex = random.nextInt(n);
        list.add(randomIndex);
    }

    public void listRemove() {
        if (list.size() == 0) {
            return;
        }
        Random random = new Random();
        int randomIndex = random.nextInt(list.size());
        list.remove(randomIndex);
    }

}
