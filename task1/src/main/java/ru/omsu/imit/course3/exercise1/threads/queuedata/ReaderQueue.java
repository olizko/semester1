package ru.omsu.imit.course3.exercise1.threads.queuedata;

import java.util.concurrent.BlockingQueue;

public class ReaderQueue extends Thread {
    private BlockingQueue<DataInterface> queue;


    public ReaderQueue(BlockingQueue<DataInterface> queue) {
        this.queue = queue;

    }


    @Override
    public void run() {
        System.out.println("Reader Start");
        try {
            boolean flag = true;
            while (flag) {
                DataInterface date = queue.take();
                if (!(date.isEnd())) {
                    System.out.println("Reader read: " + date);
                } else {
                    System.out.println("Empty queue");
                    flag=false;
                }
            }
        } catch (InterruptedException e) { }
        System.out.println("Reader finish");
    }
}
