package ru.omsu.imit.course3.exercise1.collections.institute;

public enum InstituteErrorCodes {
    WRONG_INSTITUTE_NAME("Wrong institute name"),
    WRONG_INSTITUTE_CITY("Wrong institute city");

    private String errorDetails;

    private InstituteErrorCodes(String ErrorDetails) {
        this.errorDetails = ErrorDetails;
    }

    public String getErrorDetails() {
        return errorDetails;
    }
}
