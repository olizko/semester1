package ru.omsu.imit.course3.exercise1.firsttask;


import java.util.Comparator;

/**
 * Created by Student on 11.09.2017.
 */
public class Trainee implements Comparable<Trainee> {
    private String name;//текстовая строка, не может быть null или пустой строкой
    private String surname;//текстовая строка, не может быть null или пустой строкой
    private int mark;//целое, допустимые значения от 1 до 5

    public Trainee(String name, String surname, int mark) throws TraineeException {
        if (name == null || name == "") {
            throw new TraineeException(ErrorCodes.NO_NAME_EXCEPTION);
        }
        this.name = name;
        if (surname == null || surname == "") {
            throw new TraineeException(ErrorCodes.NO_SURNAME_EXCEPTION);
        }
        this.surname = surname;
        if (mark < 0 || mark > 6) {
            throw new TraineeException(ErrorCodes.MARK_ERROR_EXCEPTION);
        }
        this.mark = mark;
    }

    public Trainee() throws TraineeException {
    }
    // getter/setter

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getMark() {
        return mark;
    }

    public void setName(String name) throws TraineeException {

        this.name = name;
    }

    public void setSurname(String surname) throws TraineeException {
        this.surname = surname;
    }

    public void setMark(int mark) throws TraineeException {
        this.mark = mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trainee trainee = (Trainee) o;

        if (mark != trainee.mark) return false;
        if (name != null ? !name.equals(trainee.name) : trainee.name != null) return false;
        return surname != null ? surname.equals(trainee.surname) : trainee.surname == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + mark;
        return result;
    }

    @Override
    public int compareTo(Trainee o) {
        int resultByLastName = getSurname().compareTo(o.getSurname());
        if (resultByLastName != 0)
            return resultByLastName;
        return getName().compareTo(o.getName());
    }
}