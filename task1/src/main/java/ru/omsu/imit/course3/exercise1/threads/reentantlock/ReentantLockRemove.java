package ru.omsu.imit.course3.exercise1.threads.reentantlock;

import ru.omsu.imit.course3.exercise1.threads.forlist.ListForThread;

import java.util.concurrent.locks.ReentrantLock;

public class ReentantLockRemove extends Thread {

    private ListForThread list;
    private ReentrantLock lock;

    public ReentantLockRemove(ListForThread list, ReentrantLock lock) {
        this.list = list;
        this.lock = lock;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                lock.lock();
                list.listRemove();
            } finally {
                lock.unlock();
            }
        }
    }
}
