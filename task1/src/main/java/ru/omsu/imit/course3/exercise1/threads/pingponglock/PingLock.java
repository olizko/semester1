package ru.omsu.imit.course3.exercise1.threads.pingponglock;

public class PingLock extends Thread{
    private PingPongLock pingPongLock;

    public PingLock(PingPongLock pingPongLock) {
        this.pingPongLock = pingPongLock;
    }

    public void run() {
        for (; ; )
            try {
                pingPongLock.ping();
            } catch (InterruptedException e) {
                System.out.println("Fail");
            }
    }
}
