package ru.omsu.imit.course3.exercise1.threads.pingponglock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class PingPongLock {
    private ReentrantLock lock = new ReentrantLock();
    private Condition conditionPing = lock.newCondition();
    private Condition conditionPong = lock.newCondition();
    private int count = 0;

    public void ping() throws InterruptedException {
        lock.lock();
        try {
            while (count == 0)
                conditionPing.await();
            System.out.println("Ping");
            count++;
            conditionPong.signal();
        } finally {
            lock.unlock();
        }
    }

    public void pong() throws InterruptedException {
        lock.lock();
        try {
            while (count != 0)
                conditionPong.await();
            System.out.println("Pong");
            count--;
            conditionPing.signal();
        } finally {
            lock.unlock();
        }
    }
}
