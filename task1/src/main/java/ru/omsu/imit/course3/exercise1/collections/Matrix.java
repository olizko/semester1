package ru.omsu.imit.course3.exercise1.collections;

import java.util.*;

public class Matrix {
    public static void FindSameLines(int[][] matrix) {
        Map<Set<Integer>, int[]> map = new HashMap<>();
        for (int[] i : matrix) {
            Set<Integer> set = new TreeSet<Integer>();
            for (int j : i) {
                set.add(j);
            }
            map.put(set, i);
        }
        Collection<int[]> col = map.values();
        for (int[] i : col) {
            for (int j : i) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}
