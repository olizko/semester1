package ru.omsu.imit.course3.exercise1.collections.group;

import ru.omsu.imit.course3.exercise1.collections.group.GroupErrorCodes;
import ru.omsu.imit.course3.exercise1.firsttask.Trainee;

public class GroupException extends Exception {

        public GroupException(Throwable cause) {
                super(cause);
        }

        public GroupException(String s) {
                super(s);
        }
        public GroupException(GroupErrorCodes wrongGroupName, String groupName1) {
                super();
        }

        public GroupException(GroupErrorCodes errorCode) {
                super(errorCode.getErrorDetails());
        }

        public GroupException(GroupErrorCodes errorCode, Trainee[] trainees1) {
                super(String.format(errorCode.getErrorDetails(), trainees1));
        }
}
