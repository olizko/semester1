package ru.omsu.imit.course3.exercise1.threads.message;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class Transport {
    public static void send(Message message) throws Exception {
        try (PrintStream traineePrint = new PrintStream(new FileOutputStream("message.txt",true), true)) {
            traineePrint.println(message.getEmailAddress() + "\n" + message.getSender() + "\n" + message.getSubject() +
                    "\n" + message.getBody());
        } catch (IOException e) {
            throw new Exception("can't send a message", e);
        }
    }
}
