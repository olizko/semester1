package ru.omsu.imit.course3.exercise1.threads.queuedata;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class WriterQueue extends Thread {
    private BlockingQueue<DataInterface> queue;
    private int count;

    public WriterQueue(BlockingQueue<DataInterface> queue, int count) {
        this.queue = queue;
        this.count = count;
    }

    @Override
    public void run() {
        System.out.println("Writer Start");
        for (int i = 0; i < count; i++) {
            try {
                queue.put(new Data(createArrayForData(count)));
                System.out.println("Writer added: element " + i);
            } catch (InterruptedException e) { }
        }
        System.out.println("Writer finish");
    }

    public int[] createArrayForData(int count){
        int[] result = new int[count];
        Random random = new Random();
        for (int i = 0; i < count; i++){
            result[i]= random.nextInt(100);
        }
        return result;


    }
}
