package ru.omsu.imit.course3.exercise1.threads.queuetask;

public class Task extends AbstractTask implements Executable  {
    private String string;

    public Task(String string){
        this.string = string;
    }


    @Override
    public void execute() {

    }

    @Override
    boolean isTerminator() {
        return false;
    }
}
