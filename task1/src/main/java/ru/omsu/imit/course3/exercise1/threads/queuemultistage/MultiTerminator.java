package ru.omsu.imit.course3.exercise1.threads.queuemultistage;

public class MultiTerminator extends AbstractMultiStage {

    @Override
    boolean isTerminator() {
        return true;
    }

    @Override
    boolean isComplete() {
        return false;
    }

    @Override
    void execution() {

    }
}
