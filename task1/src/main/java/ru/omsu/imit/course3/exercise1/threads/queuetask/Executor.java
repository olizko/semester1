package ru.omsu.imit.course3.exercise1.threads.queuetask;

import java.util.concurrent.BlockingQueue;

public class Executor extends Thread {
    private BlockingQueue<AbstractTask> blockingQueue;


    public Executor(BlockingQueue<AbstractTask> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    public void run() {
        while (true) {
            AbstractTask task = blockingQueue.poll();
            if (task == null) {
                System.out.println("Empty queue");
            } else {
                if (task.isTerminator()) {
                    break;
                }
            }
        }
    }
}
