package ru.omsu.imit.course3.exercise1.threads.queuemultistage;

import java.util.concurrent.BlockingQueue;

public class MultiDeveloper extends Thread {

    private BlockingQueue<AbstractMultiStage> concurrentLinkedQueue;

    public MultiDeveloper(BlockingQueue<AbstractMultiStage> concurrentLinkedQueue){
        this.concurrentLinkedQueue = concurrentLinkedQueue;
    }

    public void run(){
        concurrentLinkedQueue.add(new MultiStage(5));
    }
}
