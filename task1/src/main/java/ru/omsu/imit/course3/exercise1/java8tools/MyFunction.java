package ru.omsu.imit.course3.exercise1.java8tools;

public interface MyFunction<K, T> {
    K apply(T arg);
}
