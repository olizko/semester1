package ru.omsu.imit.course3.exercise1.firsttask;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TraineeArrayList {
    public static List<Trainee> reverseArrayList(List<Trainee> al) {
        Collections.reverse(al);
        return al;
    }

    public static List<Trainee> moveDoubleArrayList(List<Trainee> al) {
        LinkedList b = new LinkedList();
        b.add(0, al.get(al.size() - 2));
        b.add(1, al.get(al.size() - 1));
        for (int i = 0; i < al.size() - 2; i++) {
            b.add(al.get(i));
        }
        return b;
    }

    public static List<Trainee> randomizeArrayList(List<Trainee> al) {
        Collections.shuffle(al);
        return al;
    }

    public static Trainee findMaxMarkArrayList(List<Trainee> al) throws TraineeException {
        return Collections.max(al);
    }

    public static List<Trainee> sortByMark(List<Trainee> al) {
        Collections.sort(al, (e1, e2) -> e1.getMark() - e2.getMark());
        return al;
    }

    public static List<Trainee> sortByName(List<Trainee> al) {
        Collections.sort(al, (e1, e2) -> e1.getName().compareTo(e2.getName()));
        return al;
    }

    public static List<Trainee> findByName(List<Trainee> al, String s) {
        List<Trainee> b = new LinkedList<>();
        for (Trainee p : al) {
            if (p.getName().equals(s)) {
                b.add(p);
            }
        }
        return b;
    }

}
