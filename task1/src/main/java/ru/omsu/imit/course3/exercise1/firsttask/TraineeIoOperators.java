package ru.omsu.imit.course3.exercise1.firsttask;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import java.io.*;

public class TraineeIoOperators {
    public static Trainee traineeReadFromFileOneString(String fileName) throws TraineeException {
        Trainee trainee = new Trainee();
        try (BufferedReader bos = new BufferedReader(new FileReader(fileName))) {
            String[] ostr = bos.readLine().split(" ");
            trainee.setName(ostr[0]);
            trainee.setSurname(ostr[1]);
            trainee.setMark(Integer.parseInt(ostr[2]));
            return trainee;
        } catch (IOException e) {
            new TraineeException(e.getMessage());
        } catch (TraineeException e) {
            new TraineeException(e.getMessage());
        }
        return trainee;
    }

    public static Trainee traineeReadFromFile(String fileName) throws TraineeException {
        Trainee trainee = new Trainee();
        try (BufferedReader bos = new BufferedReader((new FileReader(fileName)))) {
            trainee.setName(bos.readLine());
            trainee.setSurname(bos.readLine());
            trainee.setMark(Integer.parseInt(bos.readLine()));
        } catch (IOException e) {
            new TraineeException(e.getMessage());
        }
        return trainee;
    }

    public static void traineeWriteToFile(Trainee trainee, String fileName) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            bw.write(trainee.toString());
        } catch (IOException e) {
            new TraineeException(e.getMessage());
        }
    }

    public static void traineeWriteToFileOneString(Trainee trainee, String fileName) {
        try (BufferedWriter bw = new BufferedWriter((new FileWriter(fileName)))) {
            bw.write(trainee.getName().toString() + " " + trainee.getSurname().toString() + " " + trainee.getMark());
        } catch (IOException e) {
            new TraineeException(e.getMessage());
        }
    }

    public static void traineeSerializeToFile(Trainee trainee, String fileName) {
        try (ObjectOutputStream ous = new ObjectOutputStream(new FileOutputStream(fileName))) {
            ous.writeObject(trainee);
        } catch (IOException e) {
            new TraineeException(e.getMessage());
        }
    }

    public static Trainee traineeDeserializeFromFile(String fileName) throws TraineeException {
        Trainee trainee = new Trainee();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            trainee = (Trainee) ois.readObject();
        } catch (IOException e) {
            new TraineeException(e.getMessage());
        } catch (ClassNotFoundException e) {
            new TraineeException(e.getMessage());
        }
        return trainee;
    }

    public static byte[] traineeSerializeToByteArrayOS(Trainee trainee) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[0];
        try (ObjectOutputStream ous = new ObjectOutputStream(new BufferedOutputStream(baos))) {
            ous.writeObject(trainee);
            ous.flush();
            buffer = baos.toByteArray();

        } catch (IOException e) {
            new TraineeException(e.getMessage());
        }
        return buffer;
    }

    public static Trainee traineeDeserializeFromByteArrayOS(byte[] buffer) throws TraineeException {
        Trainee trainee = new Trainee();
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new ByteArrayInputStream(buffer)))) {
            Object obj = ois.readObject();
            if (!(obj instanceof Trainee))
                throw new TraineeException("File has no trainee");
            trainee = (Trainee) obj;
        } catch (IOException e) {
            new TraineeException(e.getMessage());
        } catch (ClassNotFoundException e) {
            new TraineeException((e.getMessage()));
        }
        return trainee;

    }

    public static String traineeToJson(Trainee trainee) throws TraineeException {
        Gson gson = new Gson();
        return gson.toJson(trainee);
    }

    public static Trainee traineeFromJson(String jsonInString) throws TraineeException {
        Trainee trainee = null;
        try {
            Gson gson = new Gson();
            trainee = gson.fromJson(jsonInString, Trainee.class);
            return trainee;
        } catch (JsonSyntaxException e) {
            throw new TraineeException(ErrorCodes.INPUT_OUTPUT_EXCEPTION);
        }
    }

    public static void traineeToJsonFile(Trainee trainee, String fileName) throws TraineeException {
        try (FileWriter fw = new FileWriter(fileName)) {
            Gson gson = new Gson();
            gson.toJson(trainee, fw);
        } catch (JsonIOException e) {
            throw new TraineeException(ErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new TraineeException(ErrorCodes.INPUT_OUTPUT_EXCEPTION);
        }
    }

    public static Trainee traineeFromJsonFile(String fileName) throws TraineeException {
        Trainee trainee = null;
        try (FileReader fr = new FileReader(fileName)) {
            Gson gson = new Gson();
            trainee = gson.fromJson(fr, Trainee.class);
            return trainee;
        } catch (JsonSyntaxException e) {
            throw new TraineeException(ErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new TraineeException(ErrorCodes.INPUT_OUTPUT_EXCEPTION);
        }
    }
}