package ru.omsu.imit.course3.exercise1.collections.group;
enum GroupErrorCodes {
    WRONG_GROUP_NAME("Wrong group name"),
    WRONG_TRAINEES("Wrong trainees"),
    UNFOUND_GROUP_NAME("Your group name can't be found");

    private String errorDetails;

    private GroupErrorCodes(String ErrorDetails) {
        this.errorDetails = ErrorDetails;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

}
