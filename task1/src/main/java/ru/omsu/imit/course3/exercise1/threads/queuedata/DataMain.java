package ru.omsu.imit.course3.exercise1.threads.queuedata;

import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DataMain {
    public static void main(String[] args) {
        System.out.println("Enter the number of copies");
        Scanner in = new Scanner(System.in);
        int count =  in.nextInt();
        System.out.println("Enter the number of writers");
        int countWriters =  in.nextInt();
        System.out.println("Enter the number of readers");;
        int countReader =  in.nextInt();

        BlockingQueue<DataInterface> queue = new LinkedBlockingQueue<>();

        WriterQueue[] writerQueues = new WriterQueue[countWriters];
        for(int i = 0; i < countWriters; i++){
            writerQueues[i] = new WriterQueue(queue, count);
            writerQueues[i].start();
        }

        ReaderQueue[] readerQueues = new ReaderQueue[countReader];
        for(int i = 0; i < countReader; i++) {
            readerQueues[i] = new ReaderQueue(queue);
            readerQueues[i].start();
        }

        for(int i = 0; i < countWriters; i++){
            try {
                writerQueues[i].join();
            } catch (InterruptedException e) {            }
        }

        for (int k = 0; k < countReader; k++){
            queue.add(new DataFinish());
        }

        for(int i = 0; i < countReader; i++) {
            try {
                readerQueues[i].join();
            } catch (InterruptedException e) {            }
        }

    }
}
