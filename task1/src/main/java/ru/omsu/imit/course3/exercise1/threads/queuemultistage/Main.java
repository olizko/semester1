package ru.omsu.imit.course3.exercise1.threads.queuemultistage;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    @Parameter(names = {"--writer", "-w"})
    int writer;
    @Parameter(names = {"--reader", "-r"})
    int reader;

    public static void main(String... argv) {
        Main main = new Main();
        JCommander.newBuilder().addObject(main).build().parse(argv);
        main.run();
    }

    public void run() {
        if (writer == 0) {
            writer = 1;
        }
        if (reader == 0) {
            reader = 1;
        }
        BlockingQueue<AbstractMultiStage> blockingQueue = new LinkedBlockingQueue<>();
        List<MultiDeveloper> taskDeveloperList = new ArrayList<>();
        List<MultiExecutor> taskExecutorList = new ArrayList<>();
        for (int i = 0; i < writer; i++) {
            taskDeveloperList.add(new MultiDeveloper(blockingQueue));
        }
        CountDownLatch countDownLatch = new CountDownLatch(writer);
        for (int i = 0; i < reader; i++) {
            taskExecutorList.add(new MultiExecutor(blockingQueue, countDownLatch));
        }
        for (MultiDeveloper developer : taskDeveloperList) {
            developer.start();
        }

        Observer observer = new Observer(blockingQueue);
        observer.start();
        for (MultiExecutor executor : taskExecutorList) {
            executor.start();
        }
        try {
            countDownLatch.await();
            for (int i = 0; i < reader; i++) {
                blockingQueue.add(new MultiTerminator());
            }
        } catch (InterruptedException e) {
            System.out.println("Fail");
        }
        try {
            for (MultiDeveloper developer : taskDeveloperList) {
                developer.join();
            }
        } catch (InterruptedException e) {
            System.out.println("Fail");
        }


        try {
            for (MultiExecutor executor : taskExecutorList) {
                executor.join();
            }
        } catch (InterruptedException e) {
            System.out.println("Fail");
        }
        try {
            observer.join();
        } catch (InterruptedException e) {
            System.out.println("Fail");
        }

    }

}
