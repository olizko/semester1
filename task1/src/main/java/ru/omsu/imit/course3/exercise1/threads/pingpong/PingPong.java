package ru.omsu.imit.course3.exercise1.threads.pingpong;

import java.util.concurrent.Semaphore;

public class PingPong {
    private Semaphore semaphorePing = new Semaphore(0);
    private Semaphore semaphorePong = new Semaphore(1);

    public void ping() throws InterruptedException {
        try {
            semaphorePing.acquire();
            System.out.println("Ping");
        } finally {
            semaphorePong.release();
        }
    }

    public void pong() throws InterruptedException{
        try {
            semaphorePong.acquire();
            System.out.println("Pong");
        } finally {
            semaphorePing.release();
        }
    }
}
