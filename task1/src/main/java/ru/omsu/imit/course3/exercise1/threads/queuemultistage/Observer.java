package ru.omsu.imit.course3.exercise1.threads.queuemultistage;

import java.util.concurrent.BlockingQueue;

public class Observer extends Thread{
    private BlockingQueue<AbstractMultiStage> concurrentLinkedQueue;

    public Observer(BlockingQueue<AbstractMultiStage> concurrentLinkedQueue) {
        this.concurrentLinkedQueue = concurrentLinkedQueue;
    }

    public void run() {
        while (concurrentLinkedQueue.size() > 0) {
            System.out.println("working");
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Fail");
            }
        }
        System.out.println("end");
    }
}
