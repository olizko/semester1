package ru.omsu.imit.course3.exercise1.threads.formatter;

public class FormatterThread extends Thread {
    private Formatter formatter;

    public FormatterThread(Formatter formatter){
        this.formatter = formatter;
    }

    public void run(){
        Formatter.format(formatter.getDate());
    }
}
