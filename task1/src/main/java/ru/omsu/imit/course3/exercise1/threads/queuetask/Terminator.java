package ru.omsu.imit.course3.exercise1.threads.queuetask;

public class Terminator extends AbstractTask {

    public Terminator(){
    }

    @Override
    boolean isTerminator() {
        return true;
    }
}
