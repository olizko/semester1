package ru.omsu.imit.course3.exercise1.threads.queuemultistage;

import javafx.stage.Stage;
import ru.omsu.imit.course3.exercise1.threads.queuetask.Executable;

import java.util.List;

public class MultiStage extends AbstractMultiStage {

    private List<Executable> executables;
    private int stage;

    public MultiStage(int taskCount) {
        for (int i = 0; i < taskCount; i++){
            executables.add(new ru.omsu.imit.course3.exercise1.threads.queuemultistage.Stage());
        }
        stage = 0;
    }

    public void execution(){
        if (executables.size()>0){
            executables.get(0).execute();
            stage ++;
        }
    }

    public boolean isComplete() {
        if(stage == executables.size()){
            return true;
        }
        return false;
    }

    @Override
    boolean isTerminator() {
        return false;
    }

    public int getSize(){
        return executables.size();
    }
}
