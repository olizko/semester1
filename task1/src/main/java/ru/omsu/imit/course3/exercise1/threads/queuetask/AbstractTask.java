package ru.omsu.imit.course3.exercise1.threads.queuetask;

public abstract class AbstractTask {
    abstract boolean isTerminator();
}
