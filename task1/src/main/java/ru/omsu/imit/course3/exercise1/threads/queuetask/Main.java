package ru.omsu.imit.course3.exercise1.threads.queuetask;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    @Parameter(names = {"--writer", "-w"})
    int writer;
    @Parameter(names = {"--reader", "-r"})
    int reader;

    public static void main(String... argv) {
        Main main = new Main();
        JCommander.newBuilder().addObject(main).build().parse(argv);
        main.run();
    }

    public void run() {
        if (writer == 0) {
            writer = 1;
        }
        if (reader == 0) {
            reader = 1;
        }
        BlockingQueue<AbstractTask> blockingQueue = new LinkedBlockingQueue<>();
        List<Developer> taskDeveloperList= new ArrayList<>();
        List<Executor> taskExecutorList = new ArrayList<>();
        for (int i = 0; i < writer; i++) {
            taskDeveloperList.add(new Developer(blockingQueue,2));
        }
        for (int i = 0; i < reader; i++){
            taskExecutorList.add(new Executor(blockingQueue));
        }
        for (Developer developer : taskDeveloperList) {
            developer.start();
        }
        for (Executor executor : taskExecutorList) {
            executor.start();
        }
        try {
            for (Developer developer : taskDeveloperList) {
                developer.join();
            }
        } catch (InterruptedException e) {
            System.out.print(e);
        }
        for (int i = 0; i < reader; i++) {
            blockingQueue.add(new ru.omsu.imit.course3.exercise1.threads.queuetask.Terminator());
        }
        try {
            for (Executor executor : taskExecutorList) {
                executor.join();
            }
        } catch (InterruptedException e) {
            System.out.print(e);
        }


    }
}
