package ru.omsu.imit.course3.exercise1.threads.queuedata;

import java.util.Arrays;

public class Data implements DataInterface {
    private int[] data;

    public Data(int[] data) {
        this.data = data;
    }

    @Override
    public boolean isEnd() {
        return false;
    }

    @Override
    public String toString() {
        return "Data{" +
                "data=" + Arrays.toString(data) +
                '}';
    }
}
