package ru.omsu.imit.course3.exercise1.threads.readerwriter;

import java.util.concurrent.Semaphore;

public class MainReaderWriter {
    static Semaphore reader = new Semaphore(0);
    static Semaphore writer = new Semaphore(1);

    public void read() {
        try {
            reader.acquire();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        }

        System.out.println("Read");
        writer.release();
    }

    public void writer() {
        try {
            writer.acquire();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        }
        System.out.println("Write");
        reader.release();
    }
}
