package ru.omsu.imit.course3.exercise1.nio;

import ru.omsu.imit.course3.exercise1.firsttask.Trainee;
import ru.omsu.imit.course3.exercise1.firsttask.TraineeException;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;


public class ChannelAndBuffer {
    public static Trainee fromBytesBufferAndChannel(File fileName) throws TraineeException {
        try (FileChannel channel = new FileInputStream(new File(String.valueOf(fileName))).getChannel()) {
            ByteBuffer buffer = ByteBuffer.allocate((int) fileName.length());
            channel.read(buffer);
            buffer.position(0);
            byte[] arr = buffer.array();
            String s = new String(arr, "UTF8");
            String[] str = s.split(" ");
            Trainee trainee = new Trainee(str[1], str[2], Integer.parseInt(str[0]));
            return trainee;
        } catch (FileNotFoundException e) {
            throw new TraineeException(e.getMessage());
        } catch (IOException e) {
            throw new TraineeException(e.getMessage());
        }
    }

    public static Trainee fromBytesMappedBufferAndChannel(File fileName) throws TraineeException {
        try (FileChannel channel = new RandomAccessFile(new File(String.valueOf(fileName)), "rw").getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, (int) fileName.length());
            byte[] arr = new byte[(int) fileName.length()];
            buffer.position(0);
            buffer.get(arr);
            String s = new String(arr, "UTF8");
            String[] str = s.split(" ");
            Trainee trainee = new Trainee(str[1], str[2], Integer.parseInt(str[0]));
            return trainee;
        } catch (FileNotFoundException e) {
            throw new TraineeException(e.getMessage());
        } catch (IOException e) {
            throw new TraineeException(e.getMessage());
        }

    }

    public static ByteBuffer traineeToByteBuffer(Trainee trainee) throws TraineeException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(trainee);
            ByteBuffer buffer = ByteBuffer.allocate(baos.toByteArray().length);
            buffer.put(baos.toByteArray());
            return buffer;
        } catch (IOException e) {
            throw new TraineeException(e.getMessage());
        }
    }
    public static  Trainee traineeFromByteBuffer(ByteBuffer buffer) throws TraineeException {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(buffer.array())) {
            ObjectInputStream ois = new ObjectInputStream(bais);
            return (Trainee) ois.readObject();
        } catch (IOException e) {
            throw new TraineeException(e.getMessage());
        } catch (ClassNotFoundException e) {
            throw new TraineeException(e.getMessage());
        }
    }
}