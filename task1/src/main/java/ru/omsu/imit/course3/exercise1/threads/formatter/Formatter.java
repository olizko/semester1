package ru.omsu.imit.course3.exercise1.threads.formatter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {
    private Date date;

    public Formatter(Date date){
        this.date = date;
    }


    public Date getDate(){
        return date;
    }



    public static String format(Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        return simpleDateFormat.format(date);
    }
}
