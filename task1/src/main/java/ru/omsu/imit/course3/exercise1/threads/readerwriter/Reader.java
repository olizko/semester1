package ru.omsu.imit.course3.exercise1.threads.readerwriter;

public class Reader extends Thread {
    private int repeat;
    private MainReaderWriter readerWriter;

    public Reader(MainReaderWriter readerWriter, int repeat) {
        this.readerWriter = readerWriter;
        this.repeat = repeat;
        new Thread(this, "Reader").start();
    }

    public void run() {
        for (int i = 0; i < repeat; i++)
            readerWriter.writer();
    }
}
