package ru.omsu.imit.course3.exercise1.threads.queuemultistage;

import ru.omsu.imit.course3.exercise1.threads.queuetask.Executable;

public class Stage extends AbstractMultiStage implements Executable{

    private boolean flag;

    public Stage(){
        flag = false;
    }

    @Override
    public void execute() {
        flag = true;
    }

    @Override
    boolean isTerminator() {
        return false;
    }

    @Override
    boolean isComplete() {
        return false;
    }

    @Override
    void execution() {

    }
}
