package ru.omsu.imit.course3.exercise1.java8tools;

import java.util.Optional;

public class PersonWithOptional {
    private Optional<PersonWithOptional> father;
    private Optional<PersonWithOptional> mother;

    public PersonWithOptional(){
    }

    public PersonWithOptional(PersonWithOptional father, PersonWithOptional mother){
        this.father = Optional.ofNullable(father);
        this.mother = Optional.ofNullable(mother);
    }

    public Optional<PersonWithOptional> getFather() {
        return father;
    }

    public Optional<PersonWithOptional> getMother() {
        return mother;
    }
}
