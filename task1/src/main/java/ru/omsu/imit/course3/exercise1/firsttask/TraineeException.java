package ru.omsu.imit.course3.exercise1.firsttask;

/**
 * Created by Student on 11.09.2017.
 */
public class TraineeException extends Exception{
    public TraineeException(ErrorCodes errorCodes) {
        super(errorCodes.getErrorString());
    }

    public TraineeException(String traineeError){

    }
}

