package ru.omsu.imit.course3.exercise1.collections.institute;

public class InstituteException extends Exception {

    public InstituteException() {
        super();
    }
    public InstituteException(Throwable cause) {
        super(cause);
    }

    public InstituteException(String s) {
        super(s);
    }

    public InstituteException(InstituteErrorCodes errorCode, Throwable cause) {
        super(errorCode.getErrorDetails(), cause);
    }

    public InstituteException(InstituteErrorCodes errorCode) {
        super(errorCode.getErrorDetails());
    }

    public InstituteException(InstituteErrorCodes errorCode, String param) {
        super(String.format(errorCode.getErrorDetails(), param));
    }


}
