package ru.omsu.imit.course3.exercise1.java8tools.lambda;

import ru.omsu.imit.course3.exercise1.java8tools.PersonWithOptional;
import ru.omsu.imit.course3.exercise1.java8tools.PersonWithPerson;
import ru.omsu.imit.course3.exercise1.java8tools.lambda.Person;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Supplier;


public class Lamda {
    public static List split(String s) {
        Function<String, List<String>> f = s1 -> Arrays.asList(s.split(" "));
        return f.apply(s);
    }

    public static Integer count(List<String> list) {
        Function<List<String>, Integer> result = List::size;
        return result.apply(list);
    }

    public static Integer splitAndCountAndThen(String s) {
        Function<String, List<String>> f1 = s1 -> Arrays.asList(s.split(" "));
        Function<String, Integer> f2 = f1.andThen(s1 -> f1.apply(s).size());
        return f2.apply(s);
    }

    public static Integer splitAndCountCompose(String s) {
        Function<String, List<String>> f1 = s1 -> Arrays.asList(s.split(" "));
        List<String> list = f1.apply(s);
        Function<List<String>, Integer> result = List::size;
        Function<String, Integer> f2 = result.compose(f1);
        return f2.apply(s);
    }

    public static Person create(String personName) {
        Function<String, Person> f = (Person::new);
        return f.apply(personName);
    }

    public static Integer max(int a, int b) {
        BiFunction<Integer, Integer, Integer> f = Math::max;
        return f.apply(a, b);
    }

    public static Date dateNow() {
        Supplier<Date> supplier = Date::new;
        return supplier.get();
    }

    public static boolean isEven(Integer a) {
        BooleanSupplier booleanSupplier = () -> (a % 2 == 0);
        return booleanSupplier.getAsBoolean();
    }

    public static boolean areEquals(Integer a, Integer b) {
        BiFunction<Integer, Integer, Boolean> biFunction = (x, y) -> a.equals(b);
        return biFunction.apply(a, b);
    }

    public static PersonWithPerson getMothersMotherFather(PersonWithPerson person) {
        if (person.getFather() == null || person.getMother() == null) {
            return null;
        } else {
            Function<PersonWithPerson, PersonWithPerson> f1 = PersonWithPerson::getMother;
            if (f1.apply(person) == null) {
                return null;
            } else {
                Function<PersonWithPerson, PersonWithPerson> f2 = f1.andThen(PersonWithPerson::getMother);
                if (f2.apply(person) == null) {
                    return null;
                } else {
                    Function<PersonWithPerson, PersonWithPerson> f3 = f2.andThen(PersonWithPerson::getFather);
                    if (f3.apply(person) != null) {
                        return f3.apply(person);
                    } else {
                        return null;
                    }
                }
            }
        }
    }


    public static Optional getMothersMotherFather(PersonWithOptional person) {
        return person.getMother().flatMap(PersonWithOptional::getMother)
                .flatMap(PersonWithOptional::getFather);
    }
}

