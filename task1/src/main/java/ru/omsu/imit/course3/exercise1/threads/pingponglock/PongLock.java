package ru.omsu.imit.course3.exercise1.threads.pingponglock;

public class PongLock extends Thread {
    private PingPongLock pingPongLock;

    public PongLock(PingPongLock pingPongLock) {
        this.pingPongLock = pingPongLock;
    }

    public void run() {
        for (; ; )
            try {
                pingPongLock.pong();
            } catch (InterruptedException e) {
                System.out.println("Fail");
            }
    }
}
