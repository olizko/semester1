package ru.omsu.imit.course3.exercise1.java8tools.stream;

import ru.omsu.imit.course3.exercise1.java8tools.lambda.Person;

import java.util.Comparator;
import java.util.List;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Streams {
    public static IntStream transform(IntStream stream, IntUnaryOperator op) {
        return stream.map(op);
    }

    public static IntStream transformParallel(IntStream stream, IntUnaryOperator op) {
        return stream.parallel().map(op);
    }
}
