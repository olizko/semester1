package ru.omsu.imit.course3.exercise1.firsttask;
enum ErrorCodes {

    MARK_ERROR_EXCEPTION("The mark was not entered correctly"),
    NO_NAME_EXCEPTION("Name not found"),
    NO_SURNAME_EXCEPTION("Surname not found"),
    FILE_NOT_FOUND("File not found"),
    INPUT_OUTPUT_EXCEPTION("Exception in input/output");

    private final String errorString;

    private ErrorCodes(String errorString) {
        this.errorString = errorString;
    }
    public String getErrorString() {
        return errorString;
    }


}