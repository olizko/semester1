package ru.omsu.imit.course3.exercise1.threads.pingpong;


public class Ping extends Thread {
    private PingPong pingPong;

    public Ping(PingPong pingPong) {
        this.pingPong = pingPong;
    }

    public void run() {
        for (; ; )
            try {
                pingPong.ping();
            } catch (InterruptedException e) {
            }
    }
}
