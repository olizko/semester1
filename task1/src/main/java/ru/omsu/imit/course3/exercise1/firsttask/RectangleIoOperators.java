package ru.omsu.imit.course3.exercise1.firsttask;

import java.io.*;

public class RectangleIoOperators {


    public static Rectangle rectangleReadFromFile(String fileName) throws IOException {
        try (BufferedReader rff = new BufferedReader(new FileReader(fileName))) {
            Rectangle rectangle = new Rectangle();
            rectangle.setLeft(Double.parseDouble(rff.readLine()));
            rectangle.setRight(Double.parseDouble(rff.readLine()));
            rectangle.setTop(Double.parseDouble(rff.readLine()));
            rectangle.setBottom(Double.parseDouble(rff.readLine()));
            return rectangle;
        }
    }

    public static void rectangleWriteToFile(Rectangle rectangle, String fileName) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            bw.write(rectangle.toString());
        }
    }

    public static Rectangle[] fiveRectanglesWriteRead(File fileName, Rectangle[] recs) throws IOException, TraineeException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(fileName))) {
            for (int i = 0; i < recs.length; i++) {
                dos.writeDouble(recs[i].getLeft());
                dos.writeDouble(recs[i].getRight());
                dos.writeDouble(recs[i].getTop());
                dos.writeDouble(recs[i].getBottom());
            }
        }
        try (RandomAccessFile ranac = new RandomAccessFile(fileName, "r")) {
            Rectangle[] recsarr = new Rectangle[recs.length];
            for (int i = 0; i < recs.length; i++) {
                ranac.seek(Math.abs((i - recs.length + 1) * (recs.length - 1) * Double.BYTES));
                recsarr[i] = new Rectangle(ranac.readDouble(), ranac.readDouble(), ranac.readDouble(), ranac.readDouble());
            }
            return recsarr;
        }
    }

    public static void rectangleWriteToDoubleFile(Rectangle rectangle, String fileName) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(fileName))) {
            dos.writeDouble(rectangle.getLeft());
            dos.writeDouble(rectangle.getRight());
            dos.writeDouble(rectangle.getTop());
            dos.writeDouble(rectangle.getBottom());
        }
    }

    public static Rectangle rectangleReadFromDoubleFile(String fileName) throws IOException {
        try (DataInputStream dis = new DataInputStream(new FileInputStream(fileName))) {
            Rectangle rectangle = new Rectangle();
            rectangle.setLeft(dis.readDouble());
            rectangle.setRight(dis.readDouble());
            rectangle.setTop(dis.readDouble());
            rectangle.setBottom(dis.readDouble());
            return rectangle;
        }
    }


}
