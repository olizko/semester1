package ru.omsu.imit.course3.exercise1.nio;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DatToBin {
    public static void renameDatFileToBinFile(String path,String s1, String s2) throws IOException {
        Path pathDat = Paths.get(path);
        try(DirectoryStream<Path> stream  = Files.newDirectoryStream(pathDat,"*,"+s1)){
            for(Path paths : stream){
                String fileName = paths.toFile().getName().replaceFirst("," + s1,","+ s2);
                paths.toFile().renameTo(new File(path + fileName));
            }
        }
    }

}
