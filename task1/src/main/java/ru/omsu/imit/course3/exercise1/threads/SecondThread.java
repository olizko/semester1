package ru.omsu.imit.course3.exercise1.threads;

public class SecondThread implements Runnable {
    private Thread thread;

    public SecondThread() {
        thread = new Thread(this, "thread");
        System.out.println("Second thread: " + thread);
        thread.start();
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("Second Thread: " + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Second interrupted.");
        }
        System.out.println("Exit from second thread.");
    }
}
