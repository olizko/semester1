package ru.omsu.imit.course3.exercise1.collections.group;

import ru.omsu.imit.course3.exercise1.firsttask.Trainee;

import java.util.Arrays;
import java.util.Comparator;

public class FindAndSortInGroup {

    public static Group sortByMark(Group group) {
        Arrays.sort(group.getTrainees(), Comparator.comparing(Trainee::getMark));//(e1, e2) -> e1.getMark() - e2.getMark();
        return group;
    }

    public static Group sortByName(Group group) {
        Arrays.sort(group.getTrainees(),Comparator.comparing(Trainee::getName));// (e1, e2) -> e1.getName().compareTo(e2.getName()));
        return group;
    }

    public static Trainee findByName(Group group, String Name) throws GroupException {
        int length = group.getTrainees().length;
        for (int i = 0; i < length; i++) {
            if (group.getTrainees()[i].getName().compareTo(Name) == 0) {
                return group.getTrainees()[i];
            }
        }
        return null;
    }
}
