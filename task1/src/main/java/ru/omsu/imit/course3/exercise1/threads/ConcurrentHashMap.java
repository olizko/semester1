package ru.omsu.imit.course3.exercise1.threads;

import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ConcurrentHashMap {
    private Map<Object,Object> concurrentHashMap;
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public ConcurrentHashMap(Map<Object,Object> concurrentHashMap) {
        this.concurrentHashMap = concurrentHashMap;
    }

    public void addElem(Object key, Object value) {
        lock.writeLock().lock();
        try {
            concurrentHashMap.put(key, value);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Object getElem(Object key) {
        lock.readLock().lock();
        try {
            return concurrentHashMap.get(key);
        } finally {
            lock.readLock().unlock();
        }
    }
}
