package ru.omsu.imit.course3.exercise1.threads.message;

public class TransportThread extends Thread {

    private Message message;

    public TransportThread(Message message) {
        this.message = message;
    }

    public void run() {
        try {
            Transport.send(message);
        } catch (Exception e) {
        }
    }

}