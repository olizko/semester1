package ru.omsu.imit.course3.exercise1.threads;

public class FirstThread implements Runnable {
    private Thread thread;

    public FirstThread() {
        thread = new Thread(this, "first thread");
        System.out.println("First thread: " + thread);
        thread.start();
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("First thread: " + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("First interrupted.");
        }
        System.out.println("Exit from first thread.");
    }
}
