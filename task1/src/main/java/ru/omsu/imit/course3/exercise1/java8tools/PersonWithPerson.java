package ru.omsu.imit.course3.exercise1.java8tools;

public class PersonWithPerson {
    private PersonWithPerson father;
    private PersonWithPerson mother;

    private String name;

    public PersonWithPerson(String name){
        this.name = name;
    }

    public PersonWithPerson(PersonWithPerson mother, PersonWithPerson father) {
        setFather(father);
        setMother(mother);
    }


    public PersonWithPerson getFather() {
        return father;
    }

    private void setFather(PersonWithPerson father) {
        if (father == null) {
            setFather(father);
        }
        this.father = father;
    }

    public PersonWithPerson getMother() {
        return mother;
    }

    private void setMother(PersonWithPerson mother) {
        if (mother == null) {
            setMother(mother);
        }
        this.mother = mother;
    }

    public String getName() {
        return name;
    }

    public static PersonWithPerson getMothersMotherFather(PersonWithPerson person) {
        if (person.getMother().getMother().getFather() == null) {
            return null;
        }
        return person.getMother().getMother().getFather();
    }
}
