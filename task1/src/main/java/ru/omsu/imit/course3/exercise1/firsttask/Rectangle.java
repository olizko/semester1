package ru.omsu.imit.course3.exercise1.firsttask;

import java.io.Serializable;

public class Rectangle implements Serializable {
    private double left;
    private double right;
    private double top;
    private double bottom;
    public Rectangle(double left, double right, double top, double bottom){
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
    }
    public Rectangle(){}

    public double getLeft() {
        return left;
    }
    public void setLeft(double left) {
        this.left = left;
    }
    public double getRight() {
        return right;
    }
    public void setRight(double right) {
        this.right = right;
    }
    public double getTop() {
        return top;
    }
    public void setTop(double top) {
        this.top = top;
    }
    public double getBottom() {
        return bottom;
    }
    public void setBottom(double bottom) {
        this.bottom = bottom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rectangle)) return false;

        Rectangle rectangle = (Rectangle) o;

        if (Double.compare(rectangle.getLeft(), getLeft()) != 0) return false;
        if (Double.compare(rectangle.getRight(), getRight()) != 0) return false;
        if (Double.compare(rectangle.getTop(), getTop()) != 0) return false;
        return Double.compare(rectangle.getBottom(), getBottom()) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getLeft());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getRight());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getTop());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getBottom());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return String.valueOf(left + "\n" + right +"\n"+ top +"\n"+ bottom);
    }

}