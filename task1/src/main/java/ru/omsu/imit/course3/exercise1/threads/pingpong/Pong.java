package ru.omsu.imit.course3.exercise1.threads.pingpong;

public class Pong extends Thread {
    private PingPong pingPong;

    public Pong(PingPong pingPong) {
        this.pingPong = pingPong;
    }

    public void run() {
        for (; ; )
            try {
                pingPong.pong();
            } catch (InterruptedException e) {
            }
    }
}
