package ru.omsu.imit.course3.exercise1.firsttask.thread;

import org.junit.Test;
import ru.omsu.imit.course3.exercise1.threads.formatter.Formatter;
import ru.omsu.imit.course3.exercise1.threads.formatter.FormatterThread;

import java.util.Date;


import static org.junit.Assert.*;

public class FormatterTest {
    @Test
    public void formatter() {
        Date date = new Date();
        Formatter formatter = new Formatter(date);
        FormatterThread formatterThread1 = new FormatterThread(formatter);
        FormatterThread formatterThread2 = new FormatterThread(formatter);
        FormatterThread formatterThread3 = new FormatterThread(formatter);
        FormatterThread formatterThread4 = new FormatterThread(formatter);
        FormatterThread formatterThread5 = new FormatterThread(formatter);
        formatterThread1.start();
        formatterThread2.start();
        formatterThread3.start();
        formatterThread4.start();
        formatterThread5.start();
        try {
            formatterThread1.join();
            formatterThread2.join();
            formatterThread3.join();
            formatterThread4.join();
            formatterThread5.join();
        }catch (InterruptedException e){
            fail();
        }
    }
}
