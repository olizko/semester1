package ru.omsu.imit.course3.exercise1.firsttask.java8test;

import org.junit.Test;
import ru.omsu.imit.course3.exercise1.java8tools.Person;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTest {
    @Test
    public void streamTest(){
        int k = 1;
        int n = 9;
        IntStream streams = IntStream.range(k,n).map(x -> x+x);
    }

    @Test
    public void personStreamTest(){
        Collection<Person> people = new ArrayList<>();
        Collections.addAll(people, new Person("ivan" , 54),new Person("ivan" , 74),
                new Person("petr" , 24),new Person("vova" , 54),new Person("ivan" , 34));
        Stream<Person> sorted = people.stream().filter(person -> person.getAge() > 30).filter(person -> person.getName() != person.getName()).sorted();
    }
}
