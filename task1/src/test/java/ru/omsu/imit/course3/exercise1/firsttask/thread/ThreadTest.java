package ru.omsu.imit.course3.exercise1.firsttask.thread;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.exercise1.threads.FirstThread;
import ru.omsu.imit.course3.exercise1.threads.SecondThread;
import ru.omsu.imit.course3.exercise1.threads.ThirdThread;
import ru.omsu.imit.course3.exercise1.threads.forlist.ListForThread;
import ru.omsu.imit.course3.exercise1.threads.pingpong.Ping;
import ru.omsu.imit.course3.exercise1.threads.pingpong.PingPong;
import ru.omsu.imit.course3.exercise1.threads.pingpong.Pong;
import ru.omsu.imit.course3.exercise1.threads.reentantlock.ReentantLockAdd;
import ru.omsu.imit.course3.exercise1.threads.reentantlock.ReentantLockRemove;
import ru.omsu.imit.course3.exercise1.threads.synchronize.ThreadAdd;
import ru.omsu.imit.course3.exercise1.threads.synchronize.ThreadRemove;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

import static org.junit.Assert.fail;

public class ThreadTest {
    @Test
    public void streamPropertiesTest() {
        Thread thread = new Thread("test thread");
        Assert.assertEquals("test thread", thread.getName());
        System.out.println("ID: " + thread.getId());
        System.out.println("Priority: " + thread.getPriority());
    }

    @Test
    public void threadFirstCreate() {
        new FirstThread();
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("Main Thread: " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Main thread interrupted.");
        }
        System.out.println("Main thread exiting.");
    }

    @Test
    public void threeThreadCreate() {
        new FirstThread();
        new SecondThread();
        new ThirdThread();
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("Main Thread: " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Main thread interrupted.");
        }
        System.out.println("Main thread exiting.");
    }

    @Test
    public void listSynchronizedMethodTest() {
        ListForThread listForThread = new ListForThread(new ArrayList<Integer>());
        ThreadAdd threadAddToList = new ThreadAdd(listForThread);
        ThreadRemove threadRemoveFromList = new ThreadRemove(listForThread);
        threadAddToList.start();
        threadRemoveFromList.start();
        try {
            threadAddToList.join();
            threadRemoveFromList.join();
        } catch (InterruptedException e) {
            fail();
        }
    }

    @Test
    public void pingPongThread(){
        PingPong pingPong = new PingPong();
        Ping threadPing = new Ping(pingPong);
        Pong threadPong = new Pong(pingPong);
        threadPing.start();
        threadPong.start();
        try {
            threadPing.join();
            threadPong.join();
        } catch (InterruptedException e) {
            fail();
        }
    }

    @Test
    public void reentrantLock(){
        ListForThread listForThread = new ListForThread(new ArrayList<>());
        ReentrantLock reentrantLock = new ReentrantLock();
        ReentantLockAdd reentrantLockListAdd = new ReentantLockAdd(listForThread, reentrantLock);
        ReentantLockRemove reentrantLockListRemove = new ReentantLockRemove(listForThread, reentrantLock);
        reentrantLockListAdd.start();
        reentrantLockListRemove.start();
        try {
            reentrantLockListAdd.join();
            reentrantLockListRemove.join();
        }catch (InterruptedException e){
            fail();
        }
    }
}
