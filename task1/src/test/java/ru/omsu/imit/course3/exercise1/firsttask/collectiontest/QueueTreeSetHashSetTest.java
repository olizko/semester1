package ru.omsu.imit.course3.exercise1.firsttask.collectiontest;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.exercise1.firsttask.Trainee;
import ru.omsu.imit.course3.exercise1.firsttask.TraineeException;

import java.util.*;

public class QueueTreeSetHashSetTest {
    @Test
    public void TraineeQueueTest() throws Exception {
        Trainee tr1 = new Trainee("Aaaa", "Aaa", 1);
        Trainee tr2 = new Trainee("Bbbb", "Bbb", 2);
        Trainee tr3 = new Trainee("Cccc", "Ccc", 3);
        Trainee tr4 = new Trainee("Dddd", "Ddd", 4);
        Trainee tr5 = new Trainee("Eeee", "Eee", 5);
        Queue<Trainee> queue = new PriorityQueue<>();
        queue.add(tr1);
        queue.add(tr2);
        queue.add(tr3);
        queue.add(tr4);
        queue.add(tr5);
        if (queue.isEmpty()) {
            throw new Exception("Queue is empty");
        }
    }

    @Test
    public void HashSetTest() throws TraineeException {
        Set<Trainee> setTr = new HashSet<Trainee>();
        Trainee tr1 = new Trainee("Aaaa", "Aaa", 1);
        Trainee tr2 = new Trainee("Bbbb", "Bbb", 2);
        Trainee tr3 = new Trainee("Cccc", "Ccc", 3);
        Trainee tr4 = new Trainee("Dddd", "Ddd", 4);
        Trainee tr5 = new Trainee("Eeee", "Eee", 5);
        setTr.add(tr1);
        setTr.add(tr2);
        setTr.add(tr3);
        setTr.add(tr4);
        setTr.add(tr5);
        Assert.assertTrue(setTr.contains(tr1));
        for (Trainee i : setTr)
            System.out.println(i);
    }



    @Test
    public void TreeSetTest() throws TraineeException {
        Set<Trainee> setTr1 = new TreeSet<Trainee>();
        Trainee tr1 = new Trainee("Aaaa", "Aaa", 1);
        Trainee tr2 = new Trainee("Bbbb", "Bbb", 2);
        Trainee tr3 = new Trainee("Cccc", "Ccc", 3);
        Trainee tr4 = new Trainee("Dddd", "Ddd", 4);
        Trainee tr5 = new Trainee("Eeee", "Eee", 5);
        setTr1.add(tr1);
        setTr1.add(tr2);
        setTr1.add(tr3);
        setTr1.add(tr4);
        setTr1.add(tr5);
        Assert.assertTrue(setTr1.contains(tr4));
        for (Trainee i : setTr1)
            System.out.println(i);
    }
    @Test
    public void TreeSetTraineeTest() throws TraineeException {
        SortedSet<Trainee> setSortedTr1 = new TreeSet<Trainee>((p1,p2) -> {
            int firstNameCompareResult = p1.getName().compareTo(p2.getName());
            return firstNameCompareResult != 0 ? firstNameCompareResult : p1.getSurname().compareTo(p2.getSurname());
        });        Trainee tr1 = new Trainee("Aaaa", "Aaa", 1);
        Trainee tr2 = new Trainee("Bbbb", "Bbb", 2);
        Trainee tr3 = new Trainee("Bbbb", "Ccc", 3);
        Trainee tr4 = new Trainee("Bbbb", "Ddd", 4);
        Trainee tr5 = new Trainee("Eeee", "Eee", 5);
        setSortedTr1.add(tr1);
        setSortedTr1.add(tr2);
        setSortedTr1.add(tr3);
        setSortedTr1.add(tr4);
        setSortedTr1.add(tr5);
        for(Trainee i : setSortedTr1){
            if (i.getName().equals("Bbbb")) System.out.println("Surname = Bbb");
        }
        for (Trainee i : setSortedTr1)
            System.out.println(i);
    }
    @Test
    public void ListVsHashSetTest(){
        System.out.println("ArrayList");

        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 100000; i++) {
            arrayList.add(Math.round(Math.random()*100));
        }
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++){
            arrayList.get((int) Math.round(Math.random()*99999));
        }
        long finish = System.currentTimeMillis();
        System.out.println(finish - start);

        System.out.println("HashSet");

        Set<Long> setTr = new HashSet<Long>();
        for (int i = 0; i < 100000; i++) {
            setTr.add(Math.round(Math.random()*100));
        }
        long start1 = System.currentTimeMillis();
        int size = setTr.size();
        for (int i = 0; i < 100000; i++) {
            int item = new Random().nextInt(size);
            int k = 0;
            for(Object obj : setTr)
            {
                if (k == item)
                    break;
                k++;
            }
        }
        long finish1 = System.currentTimeMillis();
        System.out.println(finish1 - start1);

        System.out.println("TreeSet");

        Set<Long> TreeSet = new TreeSet<>();
        for (int i = 0; i < 100000; i++) {
            TreeSet.add(Math.round(Math.random()*100));
        }
        long start2 = System.currentTimeMillis();
        int size2 = TreeSet.size();
        for (int i = 0; i < 100000; i++) {
            int item = new Random().nextInt(size2);
            int k = 0;
            for(Object obj : TreeSet)
            {
                if (k == item)
                    break;
                k++;
            }
        }
        long finish2 = System.currentTimeMillis();
        System.out.println(finish2 - start2);
    }

}
