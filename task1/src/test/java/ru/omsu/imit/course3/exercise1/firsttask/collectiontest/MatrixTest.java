package ru.omsu.imit.course3.exercise1.firsttask.collectiontest;

import org.junit.Test;
import ru.omsu.imit.course3.exercise1.collections.Matrix;

public class MatrixTest {
    @Test
    public void MatrixTest(){
        int[][] mat = { {2,2,2,2},
                {1,2,3,4},
                {2,2,2,2},
                {5,6,7,8}};
        Matrix.FindSameLines(mat);
    }
}
