package ru.omsu.imit.course3.exercise1.firsttask;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;


/**
 * Created by Student on 11.09.2017.
 */
public class InputOutputTest {

    @Test
    public void writeAndReadToFileRectangleTest() throws IOException {
        Rectangle rec1 = new Rectangle(2, 2, 2, 2);
        RectangleIoOperators.rectangleWriteToFile(rec1, "rec2.test");
        Assert.assertEquals(rec1, RectangleIoOperators.rectangleReadFromFile("rec2.test"));
    }

    @Test
    public void writeAndReadFileOneStringTraineeTest() throws IOException, TraineeException {
        Trainee tr1 = new Trainee("Vasiliy", "Danileyko", 5);
        TraineeIoOperators.traineeWriteToFileOneString(tr1, "trainee1.test");
        Assert.assertEquals(tr1, TraineeIoOperators.traineeReadFromFileOneString("trainee1.test"));
    }

    @Test
    public void writeAndReadFileTraineeTest() throws IOException, TraineeException {
        Trainee tr2 = new Trainee("Danil", "Olizko", 3);
        TraineeIoOperators.traineeWriteToFile(tr2, "trainee2.test");
        Assert.assertEquals(tr2, TraineeIoOperators.traineeReadFromFile("trainee2.test"));
    }

    @Test
    public void fiveRecsTest() throws IOException, TraineeException {
        Rectangle[] rec = new Rectangle[5];
        for (int i = 0; i < 5; i++) {
            rec[i] = new Rectangle(i, i, i, i);
        }
        Rectangle[] invrec = new Rectangle[rec.length];
        for (int i = 0; i < 5; i++) {
            int a = Math.abs(i - rec.length + 1);
            invrec[i] = new Rectangle(a, a, a, a);
        }
        Assert.assertArrayEquals(invrec, RectangleIoOperators.fiveRectanglesWriteRead(new File("test.test"), rec));
    }

    @Test
    public void writeAndReadDoubleFileRectangle() throws IOException {
        Rectangle rec1 = new Rectangle(1, 2, 3, 4);
        RectangleIoOperators.rectangleWriteToDoubleFile(rec1, "rec3.test");
        Assert.assertEquals(rec1, RectangleIoOperators.rectangleReadFromDoubleFile("rec3.test"));
    }

    @Test
    public void serializeTest() throws TraineeException, IOException, ClassNotFoundException {
        Trainee trainee = new Trainee("Vasiliy", "Danileyko", 4);
        TraineeIoOperators.traineeSerializeToFile(trainee, "ser.test");
        Assert.assertEquals(trainee, TraineeIoOperators.traineeDeserializeFromFile("ser.test"));
    }

    @Test
    public void traineeSerializeDeserializeByteArrTest() throws TraineeException, IOException, ClassNotFoundException {
        Trainee trainee = new Trainee("Danil", "Olizko", 4);
        byte[] arr = TraineeIoOperators.traineeSerializeToByteArrayOS(trainee);
        Assert.assertEquals(trainee, TraineeIoOperators.traineeDeserializeFromByteArrayOS(arr));
    }



    @AfterClass
    public static void deleteFiles() throws FileNotFoundException {
        File parent = new File("C:/Users/Данил/Documents/sippo/semester1/ex1");
        File[] files = parent.listFiles();
        if (files != null)
            for (File file : files) {
                if (file.getName().endsWith(".test"))
                    file.delete();
            }
        parent.delete();
    }
    @Test
    public void traineeJson() throws TraineeException {
        Trainee trainee = new Trainee("Vasiliy", "Danileyko", 3);
        String s = TraineeIoOperators.traineeToJson(trainee);
        Assert.assertEquals(trainee, TraineeIoOperators.traineeFromJson(s));

    }

    @Test
    public void traineeToJsonFile() throws TraineeException, IOException {
        Trainee trainee = new Trainee("Vasiliy", "Danileyko", 4);
        TraineeIoOperators.traineeToJsonFile(trainee, "file.test");
        Assert.assertEquals(trainee, TraineeIoOperators.traineeFromJsonFile("file.test"));
    }



}
