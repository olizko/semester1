package ru.omsu.imit.course3.exercise1.firsttask.collectiontest;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.exercise1.collections.group.FindAndSortInGroup;
import ru.omsu.imit.course3.exercise1.collections.group.Group;
import ru.omsu.imit.course3.exercise1.collections.group.GroupException;
import ru.omsu.imit.course3.exercise1.firsttask.Trainee;
import ru.omsu.imit.course3.exercise1.firsttask.TraineeException;

import static org.junit.Assert.assertEquals;

public class WorkWithGroup {
    @Test
    public void SortByMarkTest() throws TraineeException, GroupException{
        Trainee tr1 = new Trainee("assss","asd",2);
        Trainee tr2 = new Trainee("bssss","asd",5);
        Trainee tr3 = new Trainee("xssss","asd",4);
        Trainee tr4 = new Trainee("dssss","asd",1);
        Trainee tr5 = new Trainee("issss","asd",3);
        Trainee[] tr = new Trainee[5];
        tr[0] = tr1;
        tr[1] = tr2;
        tr[2] = tr3;
        tr[3] = tr4;
        tr[4] = tr5;
        Trainee[] tra = new Trainee[5];
        tra[0] = tr4;
        tra[1] = tr1;
        tra[2] = tr5;
        tra[3] = tr3;
        tra[4] = tr2;
        Group gr = new Group("first", tr);
        FindAndSortInGroup.sortByMark(gr);
        assertEquals(gr.getTrainees(),tra);
    }
    @Test
    public void SortByNameTest() throws TraineeException, GroupException {
        Trainee tr1 = new Trainee("ab","ab",2);
        Trainee tr2 = new Trainee("abc","abc",5);
        Trainee tr3 = new Trainee("abcd","abcd",4);
        Trainee tr4 = new Trainee("a","a",1);
        Trainee tr5 = new Trainee("abcde","abcde",3);
        Trainee[] tr = new Trainee[5];
        tr[0] = tr1;
        tr[1] = tr2;
        tr[2] = tr3;
        tr[3] = tr4;
        tr[4] = tr5;
        Trainee[] tra = new Trainee[5];
        tra[0] = tr4;
        tra[1] = tr1;
        tra[2] = tr5;
        tra[3] = tr3;
        tra[4] = tr2;
        Group gr = new Group("first", tr);
        FindAndSortInGroup.sortByName(gr);
        assertEquals(gr.getTrainees(), tra);
    }
    @Test
    public void FindByName() throws TraineeException, GroupException {
        Trainee[] tr = new Trainee[3];
        tr[0] = new Trainee("Ivan","Ivanov",3);
        tr[1] = new Trainee("Petr","Petrov",4);
        tr[2] = new Trainee("Vasiliy", "Pupkin",2);
        Group gr = new Group("first",tr);
        assertEquals(tr[2],FindAndSortInGroup.findByName(gr,"Vasiliy"));
    }

}
