package ru.omsu.imit.course3.exercise1.firsttask;

import org.junit.Test;

import java.io.File;

public class FileTest {
    @Test
    public void FileTests(){
        File second = new File("C:/1/","1.dat");
        File first = new File ("C:/1/2");
        File third = new File("C:/1/22.dat");
        first.mkdirs();
        second.renameTo(third);
        third.delete();
        String abpt1 = first.getAbsolutePath();
        System.out.println("1"+abpt1);
        System.out.println("2"+first.isFile());
        System.out.println("3"+first.isDirectory());
        System.out.println("4"+first.exists());
        System.out.println("5"+second.isFile());
        System.out.println("6"+second.isDirectory());
        System.out.println("7"+second.exists());
        String dirPath = "/";
        File file = new File(dirPath);
        File[] files = file.listFiles();
        System.out.println(files);

    }

}
