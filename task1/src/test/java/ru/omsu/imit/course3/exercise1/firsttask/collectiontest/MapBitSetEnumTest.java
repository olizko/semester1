package ru.omsu.imit.course3.exercise1.firsttask.collectiontest;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.exercise1.collections.institute.Institute;
import ru.omsu.imit.course3.exercise1.collections.institute.InstituteException;
import ru.omsu.imit.course3.exercise1.firsttask.Trainee;
import ru.omsu.imit.course3.exercise1.firsttask.TraineeException;

import java.util.*;

public class MapBitSetEnumTest {
    @Test
    public void HashMapTest() throws TraineeException,InstituteException{
        Map<Trainee, Institute> map= new HashMap<Trainee,Institute>();
        map.put(new Trainee("Aaaa","Aaa",1), new Institute("AAAAA","AAA"));
        map.put(new Trainee("Bbbb","Bbb",2), new Institute("BBBBB","BBB"));
        map.put(new Trainee("Cccc","Ccc",3), new Institute("CCCCC","CCC"));
        map.put(new Trainee("Dddd","Ddd",4), new Institute("DDDDD","DDD"));
        map.put(new Trainee("Eeee","Eee",5), new Institute("EEEEE","EEE"));
        System.out.println("Aaaa Aaa 1 value " + map.get(new Trainee("Aaaa", "Aaa", 1)).getInstituteName() + " " + map.get(new Trainee("Aaaa", "Aaa", 1)).getInstituteCity());
        for (Trainee key : map.keySet()) {
            System.out.println("Key: " + key);
        }
    }
    @Test
    public void TreeMapTest() throws TraineeException, InstituteException{
        Map<Trainee,Institute> treeMap= new TreeMap<Trainee,Institute>();
        treeMap.put(new Trainee("Aaaa","Aaa",1), new Institute("AAAAA","AAA"));
        treeMap.put(new Trainee("Bbbb","Bbb",2), new Institute("BBBBB","BBB"));
        treeMap.put(new Trainee("Cccc","Ccc",3), new Institute("CCCCC","CCC"));
        treeMap.put(new Trainee("Dddd","Ddd",4), new Institute("DDDDD","DDD"));
        treeMap.put(new Trainee("Eeee","Eee",5), new Institute("EEEEE","EEE"));
        System.out.println("Aaaa Aaa 1 value " + treeMap.get(new Trainee("Aaaa", "Aaa", 1)).getInstituteName() + " " + treeMap.get(new Trainee("Aaaa", "Aaa", 1)).getInstituteCity());
        for (Trainee key2 : treeMap.keySet()) {
            System.out.println("Key: " + key2);
        }
        System.out.println("Map");
        for (Trainee key1 : treeMap.keySet()) {
            System.out.println(key1.getName() + " " + key1.getSurname() + " " + key1.getMark() + " " + treeMap.get(key1).getInstituteName() + " " + treeMap.get(key1).getInstituteCity());
        }
    }
    @Test
    public void TreeMapSearchTest() throws TraineeException, InstituteException {
        Map<Trainee, Institute> treeMap = new TreeMap<Trainee, Institute>();
        treeMap.put(new Trainee("Aaaa", "Aaa", 1), new Institute("AA", "AAA"));
        treeMap.put(new Trainee("Bbbb", "Bbb", 2), new Institute("BB", "BBB"));
        treeMap.put(new Trainee("Cccc", "Ccc", 3), new Institute("CC", "CCC"));
        treeMap.put(new Trainee("Dddd", "Ddd", 4), new Institute("DD", "DDD"));
        treeMap.put(new Trainee("Eeee", "Eee", 5), new Institute("EE", "EEE"));
        System.out.println("Aaaa Aaa 3 " + treeMap.get(new Trainee("Aaaa", "Aaa", 3)).getInstituteName() + " " + treeMap.get(new Trainee("Aaaa", "Aaa", 3)).getInstituteCity());
        for (Trainee key : treeMap.keySet()) {
            System.out.println("Key: " + key);
        }
        System.out.println("Map");
        for (Trainee key1 : treeMap.keySet()) {
            System.out.println(key1.getName() + " " + key1.getSurname() + " " + key1.getMark() + " " + treeMap.get(key1).getInstituteName() + " " + treeMap.get(key1).getInstituteCity());
        }
    }
    @Test
    public void BitSetTest (){
        BitSet bitSet = new BitSet();
        bitSet.set(0, 100);
        Assert.assertEquals(true, bitSet.get(88));
        bitSet.clear(88);
        Assert.assertEquals(false, bitSet.get(88));
    }
    @Test
    public void SpeedTest() {
        System.out.println("BitSet");
        BitSet bitSet = new BitSet();
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            bitSet.set(i);
        }
        long finish = System.currentTimeMillis();
        System.out.println(finish - start);
        System.out.println("HashSet");
        Set hashSet = new HashSet<Integer>();
        long start1 = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            hashSet.add(i);
        }
        long finish1 = System.currentTimeMillis();
        System.out.println(finish1 - start1);
        System.out.println("TreeSet");
        Set treeSet = new TreeSet<Integer>();
        long start2 = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            treeSet.add(i);
        }
        long finish2 = System.currentTimeMillis();
        System.out.println(finish2 - start2);
    }

    enum Color {
        RED, GREEN, BLUE
    }

    @Test
    public void EnumSetTest() {
        EnumSet<Color> setAll = EnumSet.allOf(Color.class);
        EnumSet<Color> setRed = EnumSet.of(Color.RED);
        EnumSet<Color> setRedGreen = EnumSet.range(Color.RED, Color.BLUE);
        EnumSet<Color> setNone = EnumSet.noneOf(Color.class);
    }
}
