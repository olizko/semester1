package ru.omsu.imit.course3.exercise1.firsttask.thread;

import ru.omsu.imit.course3.exercise1.threads.message.Message;
import ru.omsu.imit.course3.exercise1.threads.message.TransportThread;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MailTest {
    private static List<String> listEmailAddress(String fileName) throws Exception {
        File file = new File(fileName);
        try (BufferedReader traineeReader = new BufferedReader(new FileReader(file))) {
            if (file.length() == 0 || file == null) {
                throw new Exception("Can't read email address");
            }
            String line;
            List<String> lines = new ArrayList<String>();
            while ((line = traineeReader.readLine()) != null) {
                lines.add(line);
            }
            return lines;
        } catch (IOException e) {
            throw new Exception("Can't read email address", e);
        }
    }


    public static void main(String[] args) throws Exception {
        ExecutorService service = Executors.newCachedThreadPool();
        List<String> address = MailTest.listEmailAddress("massage.txt");
        for (String email : address) {
            Message message = new Message(email, " SENDER cat  ", " SUBJECT cat and dog",
                    " BODY cat dog cat dog cat dog");
            service.execute(new TransportThread(message));
        }
        service.shutdown();
    }
}
