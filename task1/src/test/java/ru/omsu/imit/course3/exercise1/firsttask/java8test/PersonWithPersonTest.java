package ru.omsu.imit.course3.exercise1.firsttask.java8test;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.exercise1.java8tools.PersonWithPerson;

public class PersonWithPersonTest {
    @Test
    public void getMothersMotherFatherTest() {
        PersonWithPerson mother2 = new PersonWithPerson(new PersonWithPerson("mother"), new PersonWithPerson("result"));
        PersonWithPerson mother1 = new PersonWithPerson(mother2, new PersonWithPerson("father2"));
        PersonWithPerson person1 = new PersonWithPerson(mother1, new PersonWithPerson("father"));

        Assert.assertEquals("result",PersonWithPerson.getMothersMotherFather(person1).getName());
    }
}