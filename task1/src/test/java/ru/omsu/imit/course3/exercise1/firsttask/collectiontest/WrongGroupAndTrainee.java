package ru.omsu.imit.course3.exercise1.firsttask.collectiontest;

import org.junit.Test;
import ru.omsu.imit.course3.exercise1.collections.group.Group;
import ru.omsu.imit.course3.exercise1.collections.group.GroupException;
import ru.omsu.imit.course3.exercise1.firsttask.Trainee;
import ru.omsu.imit.course3.exercise1.firsttask.TraineeException;

import static org.junit.Assert.assertEquals;

public class WrongGroupAndTrainee {
    @Test(expected = TraineeException.class)
    public void WrongTraineeTest() throws TraineeException {
        Trainee tr = new Trainee("Ivan","Ivanov",10);
        assertEquals(tr.getName(), "10");

    }
    @Test(expected = GroupException.class)
    public void WrongGroupTest() throws GroupException, TraineeException {
        Trainee[] tr = new Trainee[0];
        Group gr = new Group("",tr);
        assertEquals(gr.getGroupName(), "");
    }
}
