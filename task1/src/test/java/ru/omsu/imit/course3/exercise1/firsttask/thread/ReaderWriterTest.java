package ru.omsu.imit.course3.exercise1.firsttask.thread;

import org.junit.Test;
import ru.omsu.imit.course3.exercise1.threads.readerwriter.MainReaderWriter;
import ru.omsu.imit.course3.exercise1.threads.readerwriter.Reader;
import ru.omsu.imit.course3.exercise1.threads.readerwriter.Writer;

import static org.junit.Assert.fail;

public class ReaderWriterTest {
    @Test
    public void readerWriter() {
        MainReaderWriter readerWriter = new MainReaderWriter();
        Writer writer = new Writer(readerWriter, 10);
        Reader reader = new Reader(readerWriter, 10);
        writer.start();
        reader.start();
        try {
            writer.join();
            reader.join();
        } catch (InterruptedException e) {
            fail();
        }
    }
}
