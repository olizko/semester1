package ru.omsu.imit.course3.exercise1.firsttask.niotest;

import org.junit.Test;
import ru.omsu.imit.course3.exercise1.nio.DatToBin;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

public class DatToBinTest {
    @Test
    public void DatToBinTest() throws IOException {
        Path path1 = Paths.get("C:/Users/Данил/Documents/sippo/semester1/task1/dat.dat");
        Path path2 = Paths.get("dat.dat");
        FileSystem fs = path1.getFileSystem();
        System.out.println(fs);

        System.out.println(path1.isAbsolute());
        System.out.println(path2.isAbsolute());

        System.out.println(path1.getFileName());
        System.out.println(path1.getParent());
        System.out.println(path1.getRoot());

        Path path2Absolute = path2.toAbsolutePath();
        System.out.println("Absolute path = " + path2Absolute);
        for (Path p : path2Absolute)
            System.out.println(p);
        Iterator<Path> i = path2Absolute.iterator();
        while (i.hasNext())
            System.out.println(i.next());


        File file3 = path2Absolute.toFile();
        System.out.println(file3);

        DatToBin.renameDatFileToBinFile("C:/Users/Данил/Documents/sippo/semester1/task1/", ".dat", ".bin");
    }
}
