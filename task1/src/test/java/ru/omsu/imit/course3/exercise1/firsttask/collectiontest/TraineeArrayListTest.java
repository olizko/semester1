package ru.omsu.imit.course3.exercise1.firsttask.collectiontest;

import org.junit.Test;
import ru.omsu.imit.course3.exercise1.firsttask.Trainee;
import ru.omsu.imit.course3.exercise1.firsttask.TraineeArrayList;
import ru.omsu.imit.course3.exercise1.firsttask.TraineeException;

import java.util.ArrayList;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class TraineeArrayListTest {
    @Test
    public void traineeArrayListTest() throws TraineeException {
        ArrayList<Trainee> al = new ArrayList<>();
        al.add(new Trainee("Ivan", "Ivanov", 1));
        al.add(new Trainee("Vasiliy", "Vasiliev", 2));
        al.add(new Trainee("Petr", "Petrov", 3));
        al.add(new Trainee("Danil", "Danilov", 4));
        al.add(new Trainee("Evgaf", "Evrafov", 5));
        ArrayList<Trainee> test = new ArrayList<>();
        test.add(new Trainee("Evgaf", "Evrafov", 5));
        test.add(new Trainee("Danil", "Danilov", 4));
        test.add(new Trainee("Petr", "Petrov", 3));
        test.add(new Trainee("Vasily", "Vasiliev", 2));
        test.add(new Trainee("Ivan", "Ivanov", 1));
        assertEquals(test, TraineeArrayList.reverseArrayList(al));
    }

    @Test
    public void traineeArrayListMoveTest() throws TraineeException {
        LinkedList<Trainee> al = new LinkedList();
        al.add(new Trainee("Ivan", "Ivanov", 1));
        al.add(new Trainee("Vasiliy", "Vasiliev", 2));
        al.add(new Trainee("Petr", "Petrov", 3));
        al.add(new Trainee("Danil", "Danilov", 4));
        al.add(new Trainee("Evgaf", "Evgafov", 5));
        LinkedList<Trainee> test = new LinkedList();
        test.add(new Trainee("Danil", "Danilov", 4));
        test.add(new Trainee("Evgraf", "Evgrafov", 5));
        test.add(new Trainee("Ivan", "Ivanov", 1));
        test.add(new Trainee("Vasiliy", "B", 2));
        test.add(new Trainee("Petr", "Petrov", 3));
        assertEquals(test, TraineeArrayList.moveDoubleArrayList(al));
    }

    @Test
    public void findMaxMarkTest() throws TraineeException {
        LinkedList<Trainee> ll = new LinkedList();
        ll.add(new Trainee("Aaaa", "Aaa", 1));
        ll.add(new Trainee("Bbbb", "Bbb", 2));
        ll.add(new Trainee("Cccc", "Ccc", 3));
        ll.add(new Trainee("Dddd", "Ddd", 4));
        ll.add(new Trainee("Eeee", "Eee", 5));
        assertEquals(new Trainee("Eeee", "Eee", 5), TraineeArrayList.findMaxMarkArrayList(ll));
    }

    @Test
    public void shuffleArrayList() throws TraineeException {
        LinkedList<Trainee> ll = new LinkedList();
        ll.add(new Trainee("Aaaa", "Aaa", 1));
        ll.add(new Trainee("Bbbb", "Bbb", 2));
        ll.add(new Trainee("Cccc", "Ccc", 3));
        ll.add(new Trainee("Dddd", "Ddd", 4));
        ll.add(new Trainee("Eeee", "Eee", 5));
        assertFalse(ll.toArray().equals(TraineeArrayList.randomizeArrayList(ll).toArray()));
    }

    @Test
    public void sortByMarkTest() throws TraineeException {
        LinkedList<Trainee> ll = new LinkedList();
        ll.add(new Trainee("Aaaa", "Aaa", 1));
        ll.add(new Trainee("Bbbb", "Bbb", 2));
        ll.add(new Trainee("Cccc", "Ccc", 3));
        ll.add(new Trainee("Dddd", "Ddd", 4));
        ll.add(new Trainee("Eeee", "Eee", 5));
        LinkedList<Trainee> test = new LinkedList();
        test.add(new Trainee("Dddd", "Ddd", 4));
        test.add(new Trainee("Bbbb", "Bbb", 2));
        test.add(new Trainee("Eeee", "Eee", 5));
        test.add(new Trainee("Aaaa", "Aaa", 1));
        test.add(new Trainee("Cccc", "Ccc", 3));
        assertEquals(ll, TraineeArrayList.sortByMark(ll));
    }

    @Test
    public void sortByNameTest() throws TraineeException {
        LinkedList<Trainee> ll = new LinkedList();
        ll.add(new Trainee("Aaaa", "Aaa", 1));
        ll.add(new Trainee("Bbbb", "Bbb", 2));
        ll.add(new Trainee("Cccc", "Ccc", 3));
        ll.add(new Trainee("Dddd", "Ddd", 4));
        ll.add(new Trainee("Eeee", "Eee", 5));
        LinkedList<Trainee> test = new LinkedList();
        test.add(new Trainee("Dddd", "Ddd", 4));
        test.add(new Trainee("Bbbb", "Bbb", 2));
        test.add(new Trainee("Eeee", "Eee", 5));
        test.add(new Trainee("Aaaa", "Aaa", 1));
        test.add(new Trainee("Cccc", "Ccc", 3));
        assertEquals(ll, TraineeArrayList.sortByName(ll));
    }

    @Test
    public void ArrayAndLinkedTest() {
        LinkedList linkedList = new LinkedList();
        for (int i = 0; i < 100000; i++) {
            linkedList.add(i);
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 100000; i++) {
            arrayList.add(i);
        }
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++){
            linkedList.get((int) Math.round(Math.random()*99999));
        }
        long finish = System.currentTimeMillis();
        System.out.println(finish - start);
        long start1 = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++){
            arrayList.get((int) Math.round(Math.random()*99999));
        }
        long finish1 = System.currentTimeMillis();
        System.out.println(finish1-start1);
    }
}
