package ru.omsu.imit.course3.exercise1.firsttask;

import org.junit.Test;

import static org.junit.Assert.*;

public class TraineeTest {
    @Test
    public void getName() throws TraineeException {
        Trainee traineeA = new Trainee("Danay", "Olizko", 5);
        assertEquals(traineeA.getName(), "Danay");
    }

    @Test
    public void getSurname() throws TraineeException {
        Trainee traineeA = new Trainee("Danay", "Olizko", 5);
        assertEquals(traineeA.getSurname(), "Olizko");
    }

    @Test
    public void getMark() throws TraineeException {
        Trainee traineeA = new Trainee("Danay", "Olizko", 5);
        assertEquals(traineeA.getMark(), 5);
    }

    @Test (expected = TraineeException.class)
    public void exceptionNullNameException() throws Exception {
        Trainee trainee = new Trainee("", "Olizko", 4);
        fail();
    }

}