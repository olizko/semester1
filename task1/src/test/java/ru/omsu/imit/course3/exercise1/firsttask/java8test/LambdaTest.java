package ru.omsu.imit.course3.exercise1.firsttask.java8test;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.exercise1.java8tools.lambda.Person;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.*;

public class LambdaTest {
    @Test
    public void lambdaTest() {
        String string = "one two three";
        Function<String, List<String>> fSplit = str -> Arrays.asList(str.split(" "));
        Function<List<String>, Integer> fCount = List::size;
        Assert.assertEquals(java.util.Optional.of(3), java.util.Optional.of(fCount.apply(fSplit.apply(string))));

        Function<String, Integer> f1 = fSplit.andThen(fCount);
        Function<String, Integer> f2 = fCount.compose(fSplit);
        Assert.assertEquals(java.util.Optional.of(3), java.util.Optional.of(f1.apply(string)));
        Assert.assertEquals(java.util.Optional.of(3), java.util.Optional.of(f2.apply(string)));

        String namePerson = "Igor";
        Function<String, Person> create = Person::new;


        BiFunction<Integer, Integer, Integer> max = Math::max;
        Assert.assertEquals(java.util.Optional.of(5), java.util.Optional.of(max.apply(1, 5)));

        Supplier<Date> getCurrentDate = Date::new;

        Predicate<Integer> isEven = x -> x %2==0;
        Assert.assertTrue(isEven.test(0));

        BiPredicate<Integer, Integer> areEqual = Integer::equals;
        Assert.assertTrue(areEqual.test(5, 5));
    }
}
