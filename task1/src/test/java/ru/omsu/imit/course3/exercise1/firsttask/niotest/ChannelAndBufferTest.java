package ru.omsu.imit.course3.exercise1.firsttask.niotest;

import org.junit.Test;
import ru.omsu.imit.course3.exercise1.nio.ChannelAndBuffer;
import ru.omsu.imit.course3.exercise1.firsttask.Trainee;
import ru.omsu.imit.course3.exercise1.firsttask.TraineeException;
import ru.omsu.imit.course3.exercise1.firsttask.TraineeIoOperators;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import static org.junit.Assert.assertEquals;

public class ChannelAndBufferTest {
    @Test
    public void byteBufferAndChannelTest() throws TraineeException {
        Trainee trainee = new Trainee("Petr", "Petrov", 5);
        File file = new File("start.test");
        TraineeIoOperators.traineeWriteToFileOneString(trainee, "start.test");
        assertEquals(trainee, ChannelAndBuffer.fromBytesBufferAndChannel(file));
        file.delete();
    }

    @Test
    public void byteMappedBufferAndChannelTest() throws TraineeException {
        Trainee trainee = new Trainee("Petr", "Petrov", 5);
        File file = new File("start1.test");
        TraineeIoOperators.traineeWriteToFileOneString(trainee, "start.test");
        assertEquals(trainee, ChannelAndBuffer.fromBytesMappedBufferAndChannel(file));
        file.delete();

    }

    @Test
    public void intArrayTest() throws IOException {
        File file = new File("MappedBuffer.test");
        file.createNewFile();
        try (FileChannel channel = new RandomAccessFile(file, "rw").getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, 400);
            for (int i = 0; i < 100; i++) {
                buffer.putInt(i);
            }

        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
        DataInputStream dis = new DataInputStream(new FileInputStream(file));
        for (int i = 0; i < 100; i++) {
            System.out.println(dis.readInt());
        }
        dis.close();
        file.delete();
    }

    @Test
    public void traineeToAndFromByteBuffer() throws TraineeException {
        Trainee trainee = new Trainee("Ivan", "Ivanov", 5);
        ByteBuffer bf = ChannelAndBuffer.traineeToByteBuffer(trainee);
        assertEquals(trainee, ChannelAndBuffer.traineeFromByteBuffer(bf));
    }
}
