package ru.omsu.imit.course3.exercise1.firsttask.thread;

import org.junit.Test;
import ru.omsu.imit.course3.exercise1.threads.pingponglock.PingLock;
import ru.omsu.imit.course3.exercise1.threads.pingponglock.PingPongLock;
import ru.omsu.imit.course3.exercise1.threads.pingponglock.PongLock;

import static org.junit.Assert.fail;

public class PingPongLockTest {
    @Test
    public void pingPongLockTest(){
        PingPongLock pingPongLock = new PingPongLock();
        PingLock pingLock = new PingLock(pingPongLock);
        PongLock pongLock = new PongLock(pingPongLock);
        pingLock.start();
        pongLock.start();
        try {
            pingLock.join();
            pongLock.join();
        }catch (InterruptedException e){
            fail();
        }
    }
}
